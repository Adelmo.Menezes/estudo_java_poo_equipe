# Estudo_Java_Poo_Equipe

### Downloads:

Baixe o git para windows no link abaixo
```
[ BAIXE O GIT ](https://git-scm.com/downloads).
```

### Configuracao:

Configure seu GIT:
```
 Ao Execulta o terminal do git:
1. git init
2. git config --global user.name "Fulano de Tal"
3. git config --global user.email fulanodetal@exemplo.br
```

### Limpando o Termina

Comando para limpar o terminal
```
 Clear
```

### Directory e Clone de projeto

Passos para clonar pojeto:
```
1. cd Documentos/
2. git clone https://gitlab.com/Adelmo.Menezes/estudo_java_poo_equipe.git
3. cd estudo_java_poo_equipe
```

### Issue/Problemas/GitLab

Assinando uma Issue e criando Merge Request no GitLab
```
Nao assinar issue ja assinada.
Ao escolher uma Issue Voce deve assina-la
1. Assinar a Issue
2. Aperta Botao 'Create Merge Request'
3. COPIA o nome da Merge Criada ao apera o botao
```

### USO GIT

usando a ferramenta git
```
Sempre Antes de Criar uma nova Branch/Merge
Voce tem que voltar para a Master
0. git checkout master

vizualizar a Branche
0.1. git branch
```
```
Quando tiver na master 
Atualize com a mesma atualizacao do 
projeto que esta na nuvem/GitLab
1. git pull origin master

se quiser pegar uma branch ja criada 
troca master pelo nome da branch existente
'mas antes disso tem que volta para o passo 0.'
```
```
Para criar uma Branch
2. git checkout -b Cola_O_Nome_da_Merge_Copiada_No_Passo_Anterior
ex: git checkout -b ConcertaMetodoCalcProdutos
```
```
Use 'git status' para listar todos os arquivos
novos ou modificados que ainda não receberam commit.
3. git status
```
```
Depois de Resolver seu problema/ISSUE
adicione seus arquivos
4. git add <arquivo>
```
```
Para remover alguma alteracao do seu
git status use
5. git restore <arquivo>
```
```
Para Commitar os arquivos adcionado 
6. git commit -m "comentários das alterações"
```
```
Ultimo passo envia para o repositorio nuvem/GitLab
7. git push origin Nome_da_Sua_Branch_criada_No_Passo_1
ex: git push origin ConcertaMetodoCalcProdutos
```

### Lembrete
```
Nao esqueca de volta 
para sua master e atualizar-la
antes de iniciar os passos

0. git checkout master
1. git pull origin master


pash -> envia 
pull -> obtem
```

## Conclusão

Essas explicações, da para estudar e resolver as issues 
em equipe, com isso estudem mais sobre Git usando o link
[Link:](https://rogerdudler.github.io/git-guide/index.pt_BR.html)



**Adelmo Menezes 2020**