package estudo_java_poo_equipe;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Adelmo Menezes
 */
class Pessoa {
    
    public  String nomePes;
    public int Ano_nasc;
    public double Altura;
    private int cpf;

    public Pessoa() {}
    
    public String getNomePes() {
        return nomePes;
    }

    public void setNomePes(String nomePes) {
        this.nomePes = nomePes;
    }

    public int getAno_nasc() {
    
        //DateFormat dateFormat = new SimpleDateFormat("yyyy");
        GregorianCalendar hoje = new GregorianCalendar();
        int ano = hoje.get(Calendar.YEAR);
        return (ano - Ano_nasc);
    }

    public void setAno_nasc(int Ano_nasc) {
        this.Ano_nasc = Ano_nasc;
    }

    public double getAltura() {
        return Altura;
    }

    public void setAltura(float Altura) {
        this.Altura = Altura;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }
    
    public void imprimirDadosDePessoa(){
        System.out.println("Nome: "+getNomePes());
        System.out.println("CPF: "+getCpf());
        System.out.println("Idade: "+getAno_nasc());
        System.out.println("Altura: "+getAltura());
    }
    
}
