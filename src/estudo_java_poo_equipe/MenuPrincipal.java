
package estudo_java_poo_equipe;

import java.util.Scanner;

/**
 *
 * @author Adelmo Menezes
 */
class MenuPrincipal {
    static int imprimirMenuPrincipal(){
        Scanner leia = new Scanner(System.in);
        System.out.println("-------^------------: ");
        System.out.println("1: Criar Pessoa ");
        System.out.println("2: Inserir Pessoa Na Agenda ");
        System.out.println("3: Imprimir os dados da pessoa ");
        System.out.println("4: Inserir pessoa em um Arquivo txt ");
        System.out.println("5: Mostrar as Pessoas do arquivo txt ");
        System.out.println("6: Sair ");
        System.out.println("----------------------: ");
        System.out.println("Escolha sua opcao : ");
        int op = leia.nextInt();
        return op;
    }
    
    static void clear(){
        for (int i=0; i <= 3; i++) {
            System.out.println("");
        }
    }
}
